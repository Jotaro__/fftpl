# fftpl - Firefall Addon Template Generator #

This is a node helper module to automate the cloning and setup process of [https://bitbucket.org/Jotaro__/template](https://bitbucket.org/Jotaro__/template) 

## Features ##

 - Prompt-driven command line interface, easy to use
 - automatically clones the template repository
 - automatically runs `npm install` and `gulp setup`
 - able to work in a parent or child folder

## Requirements ##
 - Node.js installed on your system
 - Git installed on your system
 - Basic command Line usage

## Instructions ##

### 1. Installing node.js ###

Download and install [node.js](https://nodejs.org/) if you haven't already.

### 2. Installing the module ###

Run

	npm install -g git+http://bitbucket.org/Jotaro__/fftpl.git

### 3. Creating a new template ###

Open a command line window and navigate to the folder where you want to create a new project.

Run

	fftpl

This will start a wizard guiding you through the setup steps.

You will be able to either clone directly into your current folder, or create a new one inside your current path.