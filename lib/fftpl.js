#!/usr/bin/env node

var prompt = require('gulp-prompt');
var file = require('gulp-file');
var git = require('gulp-git');
var exec = require('executive');

var g_folder = "";

init();

function init(){

  var choices = [
    "inside a subfolder",
    "inside this folder",
    "abort"
  ];

  file('', '', { src: true })
  .pipe(prompt.prompt([
    {
      type: "list",
      name: "choice",
      message: "Where do you want to create the template?",
      choices: choices
    }], function(res){
      
      if(res.choice == choices[0]){
        getFolder();
      }
      
      if(res.choice == choices[1]){
        clone('.')
      }
      
      if(res.choice == choices[2]){
        return;
      }
    })
  );

}

function getFolder(){
  
  file('', '', { src: true })
  .pipe(prompt.prompt([
    {
      type: "input",
      name: "folder",
      message: "What should the folder be called?",
      default: "Template"
      
    }], function(res){
      g_folder = res.folder;
      clone(res.folder);
    })
  );
  
}

function clone(folder){
  
  git.clone('https://bitbucket.org/Jotaro__/template.git', {args : ' "' + folder + '"'}, function (err) {
    if (err) throw err;
    install(setup);
  });
  
}


function install(cb){
  console.log('running \'npm install\', please wait...');
 
  exec.interactive('npm install', {cwd:"./"+g_folder}, function(err) {
      setup(err);
  });
}

function setup(err){
  if (err) throw err;
  console.log('\n\n');
  file('', '', { src: true })
  .pipe(prompt.prompt([
     {
        type: 'confirm',
        name: 'confirm',
        message: 'Do you want to run the setup now?',
        default: true
    }
    ], function(res){
      if(res.confirm){
        exec.interactive('gulp setup', {cwd:"./"+g_folder}, function(err) {
            
        });
      }
  }));
  
  
  
}